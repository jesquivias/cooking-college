<?php get_header(); ?>;

	<div class="container-fluid imagenes-principales">
		<div class="row imagen-superior imagen">
			<div class="col-md-6 bg-primary">
				<div class="row justify-content-center align-items-center h-100">
					<div class="col-sm-8 col-md-6">
						<div class="contenido text-center text-light py-3">
							<h2 class="text-uppercase">30 Years of Experience</h2>
							<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officia dolorem, mollitia corporis itaque aperiam quam atque ducimus quidem hic iusto nesciunt ab omnis tempora. Soluta, vel. Id voluptatem blanditiis at?</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 bg-vino"></div>
		</div><!--.row-->

		<div class="row imagen-inferior imagen">
			<div class="col-md-6 bg-secondary">
				<div class="row justify-content-center align-items-center h-100">
					<div class="col-sm-8 col-md-6">
						<div class="contenido text-center py-3">
							<h2 class="text-uppercase">About Us</h2>
							<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officia dolorem, mollitia corporis itaque
								aperiam quam atque ducimus quidem hic iusto nesciunt ab omnis tempora. Soluta, vel. Id voluptatem
								blanditiis at?</p>
							<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officia dolorem, mollitia corporis itaque
								aperiam quam atque ducimus quidem hic iusto nesciunt ab omnis tempora. Soluta, vel. Id voluptatem
								blanditiis at?</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6 bg-comida"></div>
		</div><!--.row-->
	</div><!--.container-->

	<div class="container">
		<section class="nosotros mt-5">
			<h1 class="text-center mb-5 separador">Why study with us?</h2>

			<div class="row">
				<div class="col-md-4 text-center informacion">
					<img src="img/icono_chef.png" alt="icono-chef" class="img-fluid mb-3">
					<h3>Chef's Specialist</h3>
					<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit</p>
				</div><!--col-md-4-->
				<div class="col-md-4 text-center informacion">
					<img src="img/icono_vino.png" alt="icono-vino" class="img-fluid mb-3">
					<h3>Learn Wines</h3>
					<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit</p>
				</div><!--col-md-4-->
				<div class="col-md-4 text-center informacion">
					<img src="img/icono_menu.png" alt="icono-menu" class="img-fluid mb-3">
					<h3>Food and Service</h3>
					<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit</p>
				</div><!--col-md-4-->
			</div>
		</section>
	</div>

	<section class="clases mt-5 py-5">
		<h1 class="separador text-center mb-3">Next Courses</h1>

		<div class="container">
			<div class="row">
				<div class="col-md-6 col-lg-4">
					<div class="card mb-4">
						<img src="img/clase1.jpg" class="card-img-top">

						<div class="card-meta bg-primary p-3 text-light d-flex justify-content-between align-items-center">
							<p class="m-0">20 / June / 2019 15:00 hrs</p>

							<span class="badge badge-secondary p-2">$300</span>
						</div>
						<!--.card-meta-->
						<div class="card-body">
							<h3 class="card-title">Italian Food for newbies</h3>
							<p class="card-subtitle mb-2">
								Learn the basics of Italian Food with this course
							</p>
							<p class="card-text">
								Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit amet consectetur.
							</p>
							<a href="clase.html" class="btn btn-primary d-block d-md-inline">More Information</a>
						</div>
						<!--.card-body.-->
					</div>
					<!--card-->
				</div>
				<!--.col-md-6-->
				<div class="col-md-6 col-lg-4">
					<div class="card mb-4">
						<img src="img/clase2.jpg" class="card-img-top">

						<div class="card-meta bg-primary p-3 text-light d-flex justify-content-between align-items-center">
							<p class="m-0">20 / June / 2019 15:00 hrs</p>

							<span class="badge badge-secondary p-2">$300</span>
						</div>
						<!--.card-meta-->
						<div class="card-body">
							<h3 class="card-title">Planning for marriage menus</h3>
							<p class="card-subtitle mb-2">
								Learn the basics of Italian Food with this course
							</p>
							<p class="card-text">
								Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit amet consectetur.
							</p>
							<a href="clase.html" class="btn btn-primary d-block d-md-inline">More Information</a>
						</div>
						<!--.card-body.-->
					</div>
					<!--card-->
				</div>
				<!--.col-md-6-->
				<div class="col-md-6 col-lg-4">
					<div class="card mb-4">
						<img src="img/clase3.jpg" class="card-img-top">

						<div class="card-meta bg-primary p-3 text-light d-flex justify-content-between align-items-center">
							<p class="m-0">20 / June / 2019 15:00 hrs</p>

							<span class="badge badge-secondary p-2">$300</span>
						</div>
						<!--.card-meta-->
						<div class="card-body">
							<h3 class="card-title">Learn all about Cakes</h3>
							<p class="card-subtitle mb-2">
								Learn the basics of bakery and desserts.
							</p>
							<p class="card-text">
								Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit amet consectetur.
							</p>
							<a href="clase.html" class="btn btn-primary d-block d-md-inline">More Information</a>
						</div>
						<!--.card-body.-->
					</div>
					<!--card-->
				</div>
				<!--.col-md-6-->
				<div class="col-md-6 col-lg-4">
					<div class="card mb-4">
						<img src="img/clase4.jpg" class="card-img-top">

						<div class="card-meta bg-primary p-3 text-light d-flex justify-content-between align-items-center">
							<p class="m-0">20 / June / 2019 15:00 hrs</p>

							<span class="badge badge-secondary p-2">$300</span>
						</div>
						<!--.card-meta-->
						<div class="card-body">
							<h3 class="card-title">Mexican Food</h3>
							<p class="card-subtitle mb-2">
								Learn all about this cousine.
							</p>
							<p class="card-text">
								Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit amet consectetur.
							</p>
							<a href="clase.html" class="btn btn-primary d-block d-md-inline">More Information</a>
						</div>
						<!--.card-body.-->
					</div>
					<!--card-->
				</div>
				<!--.col-md-6-->
				<div class="col-md-6 col-lg-4">
					<div class="card mb-4">
						<img src="img/clase5.jpg" class="card-img-top">

						<div class="card-meta bg-primary p-3 text-light d-flex justify-content-between align-items-center">
							<p class="m-0">20 / June / 2019 15:00 hrs</p>

							<span class="badge badge-secondary p-2">$300</span>
						</div>
						<!--.card-meta-->
						<div class="card-body">
							<h3 class="card-title">Marraquesh Food</h3>
							<p class="card-subtitle mb-2">
								Learn the basic about this food.
							</p>
							<p class="card-text">
								Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit amet consectetur.
							</p>
							<a href="clase.html" class="btn btn-primary d-block d-md-inline">More Information</a>
						</div>
						<!--.card-body.-->
					</div>
					<!--card-->
				</div>
				<!--.col-md-6-->
				<div class="col-md-6 col-lg-4">
					<div class="card mb-4">
						<img src="img/clase6.jpg" class="card-img-top">

						<div class="card-meta bg-primary p-3 text-light d-flex justify-content-between align-items-center">
							<p class="m-0">20 / June / 2019 15:00 hrs</p>

							<span class="badge badge-secondary p-2">$300</span>
						</div>
						<!--.card-meta-->
						<div class="card-body">
							<h3 class="card-title">Made 10 classes os Salads</h3>
							<p class="card-subtitle mb-2">
								Tranform the ingredients basics in gourmet.
							</p>
							<p class="card-text">
								Lorem ipsum dolor sit amet consectetur adipisicing elit Lorem ipsum dolor sit amet consectetur.
							</p>
							<a href="clase.html" class="btn btn-primary d-block d-md-inline">More Information</a>
						</div>
						<!--.card-body.-->
					</div>
					<!--card-->
				</div>
				<!--.col-md-6-->
			</div>
		</div>
	</section>

	<div class="licenciatura">
		<div class="container">
			<div class="row justify-content-center align-items-center">+
				<div class="col-md-8">
					<div class="contenido text-light text-center">
						<h2>Change your future</h2>
						<p class="display-4">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
						<a href="contacto.html" class="btn btn-primary text-uppercase">More information</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php get_footer(); ?>
